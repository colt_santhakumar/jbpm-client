/*
 * Decompiled with CFR 0_102.
 * 
 * Could not load the following classes:
 *  it.sauronsoftware.base64.Base64
 *  org.apache.commons.httpclient.Header
 *  org.apache.commons.httpclient.HttpClient
 *  org.apache.commons.httpclient.HttpMethod
 *  org.apache.commons.httpclient.methods.GetMethod
 *  org.apache.commons.httpclient.methods.PostMethod
 *  org.json.JSONArray
 *  org.json.JSONObject
 */
package com.colt.novitas.client;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.httpclient.Header;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.MDC;

public class JBPMClient {
    private String baseURL = "http://localhost:8080/jbpm-console/rest/";
    private String userName = "admin";
    private String password = "admin";
    private static final String START_PROCESS_URL = "runtime/deploymentId/process/processDefId/start";
    private static final String GET_RUNNING_WORKFLOW_URL = "runtime/deploymentId/process/instance/workflowId";
    private static final String GET_HISTORY_WORKFLOW_URL = "history/instance/workflowId";
    private static final String GET_RUNNING_VARS_URL = "runtime/deploymentId/process/instance/workflowId/variable";
    private static final String GET_HISTORY_VARS_URL = "history/instance/workflowId/variable";
    
    private static final String log_unique_id = MDC.get("unique_log_id");
    
    public static void main(String[] args) {
    	
//        JBPMClient client = new JBPMClient("http://amsjbp01:8080/business-central/rest/", "jbpmadmin", "jbpmAdmin#123");
//        System.out.println(client.getVariable(6, "evc_id"));
        
        
        JBPMClient client = new JBPMClient("http://amsnov01.internal.colt.net:8080/jbpm-console/rest/", "admin", "admin");
        System.out.println(client.getVariable(729, "evc_id"));
//        
//        HashMap<String, Object> params = new HashMap<String, Object>();
//        params.put("service_connection_id", 3);
//        Integer workflowId = client.startProcess("net.colt.novitas:internal:1.0", "internal.routine", params);
//        System.out.println("workflow id: " + workflowId);
//        for (int i = 0; i < 2; ++i) {
//            System.out.println("Status: " + client.getStatus("net.colt.novitas:internal:1.0", workflowId));
//            try {
//                Thread.sleep(1000);
//                continue;
//            }
//            catch (InterruptedException var5_5) {
//                // empty catch block
//            }
//        }
//        System.out.println("variable value: " + client.getVariable(workflowId, "evc_id"));
    }
    

    public Object getVariable(Integer workflowId, String variableName) {
        String url = String.valueOf(this.baseURL) + "history/instance/workflowId/variable";
        HttpClient client = null;
        try {
            client = new HttpClient();
            String authString = String.valueOf(this.userName) + ":" + this.password;
            byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            url = url.replaceFirst("workflowId", String.valueOf(workflowId));
            url = String.valueOf(url) + "/" + variableName;
            GetMethod method = new GetMethod(url);
            method.addRequestHeader(new Header("Accept", "application/json"));
            method.addRequestHeader("Authorization", "Basic " + authStringEnc);
            System.out.println(log_unique_id+"\nSending 'GET' request to URL : " + url);
            int responseCode = client.executeMethod((HttpMethod)method);
            System.out.println(log_unique_id+"Response Code : " + responseCode);
            if (responseCode == 200 || responseCode == 201) {
                JSONObject paretnJsonObje = this.getJSONObject(method.getResponseBodyAsStream());
                JSONArray jsonArray = paretnJsonObje.getJSONArray("historyLogList");
                
                JSONObject jsonObjectTop = jsonArray.getJSONObject(0); 
                
                JSONObject instanceLogObj;
                if (jsonObjectTop.has("variable-instance-log")) {
                	instanceLogObj = jsonObjectTop.getJSONObject("variable-instance-log");
                }
                else {
                	instanceLogObj = jsonObjectTop.getJSONObject("org.kie.services.client.serialization.jaxb.impl.audit.JaxbVariableInstanceLog");
                }
                
                if (!instanceLogObj.get("value").toString().equals("null")) {
                    return instanceLogObj.get("value").toString();
                }
                
                
            } else {
                System.out.println(log_unique_id+"Error Response. Code : " + responseCode);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getStatus(String deploymentId, Integer workflowId) {
        Integer processStateCode = this.getRunningStatus(deploymentId, workflowId);
        if (processStateCode == null) {
            System.out.println(log_unique_id+"No process code returned from runtime. Trying with history");
            processStateCode = this.getHistoryStatus(workflowId);
        }
        System.out.println(log_unique_id+"Status code returned : " + processStateCode);
        String finalProcessState = null;
        finalProcessState = processStateCode == 0 ? "PENDING" : (processStateCode == 1 ? "ACTIVE" : (processStateCode == 2 ? "COMPLETED" : (processStateCode == 3 ? "ABORTED" : (processStateCode == 4 ? "SUSPENDED" : "NA"))));
        return finalProcessState;
    }

    protected Integer getHistoryStatus(Integer workflowId) {
        String url = String.valueOf(this.baseURL) + "history/instance/workflowId";
        HttpClient client = null;
        try {
            client = new HttpClient();
            String authString = String.valueOf(this.userName) + ":" + this.password;
            byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            url = url.replaceFirst("workflowId", String.valueOf(workflowId));
            GetMethod method = new GetMethod(url);
            method.addRequestHeader(new Header("Accept", "application/json"));
            method.addRequestHeader("Authorization", "Basic " + authStringEnc);
            System.out.println(log_unique_id+"\nSending 'GET' request to URL : " + url);
            int responseCode = client.executeMethod((HttpMethod)method);
            System.out.println(log_unique_id+"Response Code : " + responseCode);
            if (responseCode == 200 || responseCode == 201) {
                JSONObject jsonObj = this.getJSONObject(method.getResponseBodyAsStream());
                if (!jsonObj.get("status").toString().equals("null")) {
                    return Integer.parseInt(jsonObj.get("status").toString());
                }
            } else {
                System.out.println(log_unique_id+"Error Response. Code : " + responseCode);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    protected Integer getRunningStatus(String deploymentId, Integer workflowId) {
        String url = String.valueOf(this.baseURL) + "runtime/deploymentId/process/instance/workflowId";
        HttpClient client = null;
        try {
            client = new HttpClient();
            String authString = String.valueOf(this.userName) + ":" + this.password;
            byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            url = url.replaceFirst("deploymentId", deploymentId);
            url = url.replaceFirst("workflowId", String.valueOf(workflowId));
            GetMethod method = new GetMethod(url);
            method.addRequestHeader(new Header("Accept", "application/json"));
            method.addRequestHeader("Authorization", "Basic " + authStringEnc);
            System.out.println(log_unique_id+"\nSending 'GET' request to URL : " + url);
            int responseCode = client.executeMethod((HttpMethod)method);
            System.out.println(log_unique_id+"Response Code : " + responseCode);
            if (responseCode == 200 || responseCode == 201) {
                JSONObject jsonObj = this.getJSONObject(method.getResponseBodyAsStream());
                if (!jsonObj.get("status").toString().equals("null")) {
                    return Integer.parseInt(jsonObj.get("status").toString());
                }
            } else {
                System.out.println(log_unique_id+"Error Response. Code : " + responseCode);
            }
        }
        catch (Exception e) {
            System.out.println(log_unique_id+"WARNING - failed to get status from runtime. Error : " + e.getMessage());
        }
        return null;
    }

    public Integer startProcess(String deploymentId, String processDefId, Map<String, Object> params) {
        String url = String.valueOf(this.baseURL) + "runtime/deploymentId/process/processDefId/start";
        Integer workflowId = null;
        HttpClient client = null;
        try {
            client = new HttpClient();
            String authString = String.valueOf(this.userName) + ":" + this.password;
            byte[] authEncBytes = Base64.encodeBase64((byte[])authString.getBytes());
            String authStringEnc = new String(authEncBytes);
            url = url.replaceFirst("deploymentId", deploymentId);
            url = url.replaceFirst("processDefId", processDefId);
            if (!params.isEmpty()) {
                url = String.valueOf(url) + "?";
                for (Map.Entry<String, Object> entry : params.entrySet()) {
                    url = String.valueOf(url) + "map_" + entry.getKey() + "=" + entry.getValue() + "&";
                }
            }
            PostMethod method = new PostMethod(url);
            method.addRequestHeader(new Header("Accept", "application/json"));
            method.addRequestHeader("Authorization", "Basic " + authStringEnc);
            System.out.println(log_unique_id+"\nSending 'GET' request to URL : " + url);
            int responseCode = client.executeMethod((HttpMethod)method);
            System.out.println(log_unique_id+"Response Code : " + responseCode);
            if (responseCode == 200 || responseCode == 201) {
                JSONObject jsonObj = this.getJSONObject(method.getResponseBodyAsStream());
                if (!jsonObj.get("id").toString().equals("null")) {
                    workflowId = Integer.parseInt(jsonObj.get("id").toString());
                }
            } else {
                System.out.println(log_unique_id+"Error Response. Code : " + responseCode);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println(log_unique_id+"Created workflow. Id: " + workflowId);
        return workflowId;
    }

    private JSONObject getJSONObject(InputStream inputStream) throws Exception {
        BufferedReader responseReader = null;
        try {
            String inputLine;
            responseReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer response = new StringBuffer();
            while ((inputLine = responseReader.readLine()) != null) {
                response.append(inputLine);
            }
            responseReader.close();
            System.out.println(response.toString());
            JSONObject jSONObject = new JSONObject(response.toString());
            return jSONObject;
        }
        finally {
            if (responseReader != null) {
                responseReader.close();
            }
        }
    }

    private JSONArray getJSONArray(InputStream inputStream) throws Exception {
        BufferedReader responseReader = null;
        try {
            String inputLine;
            responseReader = new BufferedReader(new InputStreamReader(inputStream));
            StringBuffer response = new StringBuffer();
            while ((inputLine = responseReader.readLine()) != null) {
                response.append(inputLine);
            }
            responseReader.close();
            System.out.println(response.toString());
            JSONArray jSONArray = new JSONArray(response.toString());
            return jSONArray;
        }
        finally {
            if (responseReader != null) {
                responseReader.close();
            }
        }
    }

    public JBPMClient() {
    }

    public JBPMClient(String baseURL) {
        this.baseURL = baseURL;
    }

    public JBPMClient(String baseURL, String userName, String password) {
        this.baseURL = baseURL;
        this.userName = userName;
        this.password = password;
    }
}
